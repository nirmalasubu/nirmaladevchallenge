﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace UI.Areas.DevChallenge
{
    internal static class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundleCollection)
        {
            #region STYLE BUNDLES

            Bundle cssStyleBundle = new Bundle("~/devChallenge/css/app")
                .Include("~/Areas/DevChallenge/Content/Styles/Vendors/Bootstrap/bootstrap-{version}.css")
                .Include("~/Areas/DevChallenge/Content/Styles/Vendors/Bootstrap/bootstrap-select.css")
             .Include("~/Areas/DevChallenge/Content/Styles/DevChallenge.css");
            bundleCollection.Add(cssStyleBundle);

            #endregion

            #region SCRIPT BUNDLES

            bundleCollection
                .Add(new ScriptBundle("~/devChallenge/js/jquery", "//code.jquery.com/jquery-1.11.0.min.js")
                .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/jQuery/jquery-{version}.js"));

            bundleCollection
                 .Add(new ScriptBundle("~/devChallenge/js/bootstrap", "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js")
                 .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/Bootstrap/bootstrap-{version}.js"));

            bundleCollection
                .Add(new ScriptBundle("~/devChallenge/js/vendors")
                    .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/jQueryUI/jquery-{version}.js")                    
                    .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/Angular/angular-route-{version}.js")
                    .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/Angular/angular-resource-{version}.js")
                    .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/Sanitize/angular-sanitize-{version}.js")
                    .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/Bootstrap/ui-bootstrap-tpls-{version}.js")                  
                  );

            bundleCollection
                 .Add(new ScriptBundle("~/devChallenge/js/angular", "//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.min.js")
                 .Include("~/Areas/DevChallenge/Content/Scripts/Vendors/Angular/angular-{version}.js"));

            bundleCollection
               .Add(new ScriptBundle("~/devChallenge/js/app")
                .Include("~/Areas/DevChallenge/Content/Scripts/App/app.js")
             .Include("~/Areas/DevChallenge/Content/Scripts/App/Controller/DevChallengectrl.js")
              .Include("~/Areas/DevChallenge/Content/Scripts/App/Service/DevChallengeService.js")
             
              );

            //bundleCollection
            //.Add(new ScriptBundle("~/DevChallenge/js/home")
            //.Include("~/Areas/DevChallenge/Content/Scripts/Frontiers/App/Controllers/DevChallengeHome-Controller.js"));

            #endregion
        }
    }
}
