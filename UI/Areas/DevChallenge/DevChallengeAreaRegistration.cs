﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace UI.Areas.DevChallenge
{
    public class DevChallengeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DevChallenge";
            }
        }

        //public override void RegisterArea(AreaRegistrationContext context) 
        //{
        //    context.MapRoute(
        //        "DevChallenge_default",
        //        "DevChallenge/{controller}/{action}/{id}",
        //        new { action = "Index", id = UrlParameter.Optional }
        //    );
        //}



        public override void RegisterArea(AreaRegistrationContext context)
        {
            RegisterBundles();

            //context.MapRoute(
            //    "ResearchTopic_default",
            //    "ResearchTopic/{controller}/{action}/{id}",
            //    new {action = "Index", id = UrlParameter.Optional}
            //    );
        }

        private void RegisterBundles()
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}