﻿devChallengeApp.factory('devChallengeService',
    [
        '$http', function($http) {
            var service = {};
            service.getTitles = function (titlename) {
                var promise = $http({
                    method: 'GET',
                    url: 'api/search/title/' + titlename                   
                })
                    .then(function(response) {
                        return response.data;
                    });
                return promise;
            }
            return service;
        }
            ]);
     