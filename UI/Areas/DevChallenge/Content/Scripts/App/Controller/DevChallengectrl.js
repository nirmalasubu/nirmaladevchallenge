﻿devChallengeApp.controller('devChallengecontroller',
[
    '$scope', '$routeParams','$http'  , function ($scope, $routeParams, $http) {
        init();

        function init()
        {
            $scope.Title = {};
            $scope.hasRecords = true;
            $scope.noRecords = true;
            $scope.text = 'Search';
            $scope.activeTab = 'details';
        }

        $scope.search = function () {
            $scope.text = '<i class="fa fa-spinner fa-spin"></i> Please wait...';
            $scope.width = 130;
            $http.get('/api/search/title/' + $scope.Title.TitleName)         
                               .success(function (response) {
                                   console.log(response);
                                   if (response == null)
                                   {
                                       $scope.text = 'Search';
                                       $scope.hasRecords = true;
                                       $scope.noRecords = false;
                                   }
                                   else {
                                       $scope.text = 'Search';
                                       $scope.hasRecords = false;
                                       $scope.noRecords = true;
                                       $scope.activeTab = 'details';
                                       $scope.Title = response;
                                    
                                   }
                                  

                               });

        }

        $scope.openDetail=function(filter)
        {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
    }
]);