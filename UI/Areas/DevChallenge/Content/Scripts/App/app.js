﻿var devChallengeApp = angular.module("devChallengeApp", [
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap', 
    'ngResource'
    
   
]);


devChallengeApp.config([
      '$routeProvider',
    function ( $routeProvider) {
       

        $routeProvider
            .when('/devchallenge', {
                controller: 'devChallengecontroller',
                templateUrl: 'templates/devChallengeUI.html',
                caseInsensitiveMatch: true
            })           
            .when('/', // default
                {
                    redirectTo: '/devchallenge'
                });

     
    }


]);




