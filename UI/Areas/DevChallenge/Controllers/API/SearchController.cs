﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;
using UI.Areas.DevChallenge.ViewModels;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Builders;
using System.Threading.Tasks;

namespace UI.Areas.DevChallenge.Controllers.API
{

    public class SearchController : ApiController
    {

        #region PUBLIC ACTION METHODS
        [HttpGet, Route("api/search/title/{name}")]
        public async Task<List<TitleViewModel>> GetTitle(string name)

        {
            List<TitleViewModel> titles = new List<TitleViewModel>();
            try
            {
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;

                var client = new MongoClient(connectionString);
                var database = client.GetDatabase("dev-challenge");
                IMongoCollection<Titles> collection = database.GetCollection<Titles>("Titles");


                FilterDefinition<Titles> namefilter = Builders<Titles>.Filter.Eq(x => x.TitleName, name);
                var result = collection.Find(namefilter);
                List<Titles> lstTitle = new List<Titles>();
                if (result != null)
                {
                    lstTitle = await result.ToListAsync();
                }

                if (lstTitle.Any())
                {
                    foreach (Titles mdotitles in lstTitle)
                    {
                        TitleViewModel title = new TitleViewModel();
                        title.TitleId = mdotitles.TitleId;
                        title.TitleName = mdotitles.TitleName;
                        title.ReleaseYear = mdotitles.ReleaseYear;
                        title.Storylines = MapStorylinesToViewModel(mdotitles.Storylines);
                        title.Awards = MapAwardsToViewModel(mdotitles.Awards);
                        title.Genres = MapGeneresToViewModel(mdotitles.Genres);
                        title.OtherNames = MapOtherNamesToViewModel(mdotitles.OtherNames);
                        titles.Add(title);
                    }
                }
                else
                {
                    return null;
                }
                return titles;
            }
            catch (Exception e)
            {

            }

            return titles;
        }

       
        #endregion

        #region PRIVATE MAPPING METHODS
        private List<StorylineViewModel> MapStorylinesToViewModel(Storyline[] storylines)
        {
            if (storylines == null)
            {
                return null;
            }

            List<StorylineViewModel> lststories = new List<StorylineViewModel>();
            foreach (Storyline story in storylines)
            {
                StorylineViewModel storyline = new StorylineViewModel();
                storyline.Description = story.Description;
                storyline.Language = story.Language;
                lststories.Add(storyline);
            }
            return lststories;
        }

        private List<AwardDetailViewModel> MapAwardsToViewModel(AwardDetail[] awards)
        {
            if (awards == null)
            {
                return null;
            }
            List<AwardDetailViewModel> lstawards = new List<AwardDetailViewModel>();
            foreach(AwardDetail award in awards)
            {
                AwardDetailViewModel awardDetail = new AwardDetailViewModel();
                awardDetail.Award = award.Award;
                awardDetail.AwardCompany = award.AwardCompany;
                awardDetail.AwardWon = award.AwardWon;
                awardDetail.AwardYear = award.AwardYear;
                lstawards.Add(awardDetail);
            }
            return lstawards;
        }

        private List<OtherNameViewModel> MapOtherNamesToViewModel(Othername[] otherNames)
        {
            if (otherNames == null)
            {
                return null;
            }
            List<OtherNameViewModel> lstOtherNames = new List<OtherNameViewModel>();
            foreach (Othername othername in otherNames)
            {
                OtherNameViewModel otherName = new OtherNameViewModel();
                otherName.TitleNameLanguage = othername.TitleNameLanguage;
                otherName.TitleNameType = othername.TitleNameType;
                otherName.TitleName = othername.TitleName;

                lstOtherNames.Add(otherName);
            }
            return lstOtherNames;
        }

        private List<string> MapGeneresToViewModel(string[] genres)
        {
          if(genres==null)
            {
                return null;
            }
            List<string> lstgenres = new List<string>();
            foreach(string genre in genres)
                {
                lstgenres.Add(genre);
            }
            return lstgenres;
        }

        #endregion

        #region MONGO BSON OBJECT

        [Serializable]
        [BsonIgnoreExtraElements(Inherited = true)]
        public class Titles
        {
            [BsonRepresentation(BsonType.ObjectId)]
            public string Id { get; set; }
            public AwardDetail[] Awards { get; set; }
            public string[] Genres { get; set; }
            public Othername[] OtherNames { get; set; }
            public Participant[] Participants { get; set; }
            public int ReleaseYear { get; set; }
            public Storyline[] Storylines { get; set; }
            public int TitleId { get; set; }
            public string TitleName { get; set; }
            public string TitleNameSortable { get; set; }

        }

        public class AwardDetail
        {
            public bool AwardWon { get; set; }
            public int AwardYear { get; set; }
            public string[] Participants { get; set; }
            public string Award { get; set; }
            public string AwardCompany { get; set; }
        }

        public class Othername
        {
            public string TitleNameLanguage { get; set; }
            public string TitleNameType { get; set; }
            public string TitleNameSortable { get; set; }
            public string TitleName { get; set; }
        }

        public class Participant
        {
            public int SortOrder { get; set; }
            public bool IsKey { get; set; }
            public string RoleType { get; set; }
            public bool IsOnScreen { get; set; }
            public string ParticipantType { get; set; }
            public string Name { get; set; }
            public int ParticipantId { get; set; }
        }

        public class Storyline
        {
            public string Description { get; set; }
            public string Language { get; set; }
            public string Type { get; set; }
        }

        #endregion
    }
    }
