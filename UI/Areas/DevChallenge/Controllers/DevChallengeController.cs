﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Areas.DevChallenge.ViewModels;

namespace UI.Areas.DevChallenge.Controllers
{
    public class DevChallengeController : Controller
    {
        // GET: DevChallenge/DevChallenge
        public ActionResult Index()
        {
            TitleViewModel titleviewModel = new TitleViewModel();
            return View("~/Areas/DevChallenge/Views/Index.cshtml", titleviewModel);
        }
    }
    }