﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Areas.DevChallenge.ViewModels
{
    public class AwardDetailViewModel
    {
        public bool AwardWon { get; set; }
        public int AwardYear { get; set; }
        public string[] Participants { get; set; }
        public string Award { get; set; }
        public string AwardCompany { get; set; }
    }
}