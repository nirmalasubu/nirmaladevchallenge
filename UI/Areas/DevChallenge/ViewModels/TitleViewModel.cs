﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.Areas.DevChallenge.Controllers.API;

namespace UI.Areas.DevChallenge.ViewModels
{
    public class TitleViewModel
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public  int ReleaseYear { get; set; }
        public List<AwardDetailViewModel>Awards { get; set; }
        public List<string> Genres { get; set; }
        public List<OtherNameViewModel> OtherNames { get; set; }
        public List<StorylineViewModel> Storylines { get; set; }
    }
}